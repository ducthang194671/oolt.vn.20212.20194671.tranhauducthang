import javax.swing.JOptionPane;

public class ShowTwoNumber {
    public static void main(String[] args) {
        String num1, num2;
        String notifMsg = "You're just enterd : ";
        num1 = JOptionPane.showInputDialog(null, "Num1", "Input first number : ", JOptionPane.INFORMATION_MESSAGE);
        notifMsg += num1 + " and ";
        num2 = JOptionPane.showInputDialog(null, "Num2", "Input second number : ", JOptionPane.INFORMATION_MESSAGE);
        notifMsg += num2;
        JOptionPane.showMessageDialog(null, notifMsg, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);

    }
}
