import java.util.Scanner;

public class App {
    static int checkMonth(String month) {
        // System.out.println(month.equals("1"));
        if (month.equals("1") || (month.equals("January")) || month.equals("Jan.") || month.equals("Jan")) {
            // System.out.println("thang 1");
            return 1;
        } else if (month.equals("2") || month.equals("February") || month.equals("Feb.") || month.equals("Feb"))
            return 2;
        else if (month.equals("3") || month.equals("March") || month.equals("Mar.") || month.equals("Mar"))
            return 3;
        else if (month.equals("4") || month.equals("April") || month.equals("Apr.") || month.equals("Apr"))
            return 4;
        else if (month.equals("5") || month.equals("May"))
            return 5;
        else if (month.equals("6") || month.equals("June") || month.equals("Jun"))
            return 6;
        else if (month.equals("7") || month.equals("July") || month.equals("Jul"))
            return 7;
        else if (month.equals("8") || month.equals("August") || month.equals("Aug.") || month.equals("Aug"))
            return 8;
        else if (month.equals("9") || month.equals("September") || month.equals("Sept.") || month.equals("Sep"))
            return 9;
        else if (month.equals("10") || month.equals("October") || month.equals("Oct.") || month.equals("Oct"))
            return 10;
        else if (month.equals("11") || month.equals("November") || month.equals("Nov.") || month.equals("Nov"))
            return 11;
        else if (month.equals("12") || month.equals("December") || month.equals("Dec.") || month.equals("Dec"))
            return 12;
        else
            return 0;
    }

    public static void main(String[] args) throws Exception {
        boolean isLeapYear;
        int month;
        month = 0;
        // System.out.println(checkMonth("1"));
        Scanner keyboard = new Scanner(System.in);
        do {
            // System.out.println(month);
            System.out.println("Enter month : ");
            String inputMonth = keyboard.nextLine();
            // System.out.println("check: " + App.checkMonth(inputMonth));
            month = checkMonth(inputMonth);
        } while (month == 0);
        int year = 0;
        do {
            System.out.println("Enter year : ");
            year = keyboard.nextInt();
        } while (year <= 0);

        if (year % 4 != 0)
            isLeapYear = false;
        else if (year % 100 != 0)
            isLeapYear = true;
        else if (year % 400 != 0)
            isLeapYear = false;
        else
            isLeapYear = true;

        if (month == 2) {
            if (isLeapYear)
                System.out.println(month + "/" + year + " has 29days");
            else
                System.out.println(month + "/" + year + " has 28days");

        } else if (month <= 7) {
            if (month % 2 == 0)
                System.out.println(month + "/" + year + " has 30days");
            else
                System.out.println(month + "/" + year + " has 31days");
        } else {
            if (month % 2 == 0)
                System.out.println(month + "/" + year + " has 31days");
            else
                System.out.println(month + "/" + year + " has 30days");
        }
        keyboard.close();
    }
}
