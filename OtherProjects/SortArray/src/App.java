import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner keyboard = new Scanner(System.in);
        int n = 0;
        do {
            System.out.println("Input n (lengh of array): ");
            n = keyboard.nextInt();
        } while (n <= 0);
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            int x = keyboard.nextInt();
            arr[i] = x;
        }

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] > arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        for (int i : arr)
            System.out.print(i + " ");
        keyboard.close();
    }
}
