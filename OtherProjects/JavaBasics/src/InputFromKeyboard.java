import java.util.Scanner;

public class InputFromKeyboard {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Name");
        String name = keyboard.nextLine();
        System.out.println("Age");
        int iAge = keyboard.nextInt();
        System.out.println("Height");
        double dHeight = keyboard.nextDouble();
        System.out.println("Mrs/Ms. " + name + ", " + iAge + " years old." + "Your height is " + dHeight + ".");
        keyboard.close();
    }
}
