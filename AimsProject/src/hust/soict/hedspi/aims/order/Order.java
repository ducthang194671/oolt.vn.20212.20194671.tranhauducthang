package hust.soict.hedspi.aims.order;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.utils.MyDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    public static final int DVD_MEDIA_TYPE = 1;
    public static final int CD_MEDIA_TYPE = 2;
    public static final int BOOK_MEDIA_TYPE = 3;
    private static int nbOrders = 0;

    private List<Media> itemsOrdered = new ArrayList<>();

    private Order() {
    }

    public List<Media> getItemsOrdered() {
        return itemsOrdered;
    }

    public static Order createOrder() {
        if (nbOrders < MAX_LIMITED_ORDERS) {
            nbOrders++;
            System.out.println("A new order has been created successfully");
            return new Order();
        } else {
            System.out.println("You have reached max orders");
            return null;
        }
    }

    public void addMedia(Media media) {
        if (itemsOrdered.size() >= MAX_NUMBERS_ORDERED) {
            System.out.println("Your order has been full !!!");
            return;
        }
      itemsOrdered.add(media);
      System.out.println("Add item to order successfully !!!");
    }

    public void removeMedia(int index) {
        if (index < 0 || index >= itemsOrdered.size()) {
            System.out.println("Index is not valid !!!");
            return;
        }
        itemsOrdered.remove(index);
        System.out.println("Remove item from order successfully !!!");
    }

    public float totalCost() {
        float sum = 0;
        for (Media item :
                itemsOrdered) {
            sum += item.getCost();
        }
        return sum;
    }

    public void print() {
        System.out.println("***********************Order***********************");
        System.out.println("Date : " + new MyDate());
        // TO DO
        int index = 1;
        for (Media item :
                itemsOrdered) {
            System.out.println(index++ + ". " + item.toString());
        }
        System.out.println("Total cost : " + totalCost());
        System.out.println("***************************************************");
    }

    public void findByTitle(String title) {
        // TO DO
    }

    public void deleteItemById() {
        System.out.println("Please input item's index you want to delete : ");
        Scanner sc = new Scanner(System.in);
        int id = sc.nextInt();
        sc.nextLine();
        if (id < 1 || id > itemsOrdered.size()) {
            System.out.println("Please input valid index !!! ");
        } else {
            itemsOrdered.remove(id - 1);
            System.out.println("Deleted successfully");
        }
    }

    public Media getALuckyItem() {
//		Random ran = new Random();
//		int luckyNumber = ran.nextInt(qtyDvdOrdered);
//		System.out.println("Get a lucky item : ");
        return itemsOrdered.get(new Random().nextInt(0, itemsOrdered.size()));
    }
}
