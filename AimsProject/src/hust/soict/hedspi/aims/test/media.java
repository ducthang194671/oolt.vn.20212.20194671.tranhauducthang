package hust.soict.hedspi.aims.test;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;

import java.util.*;

public class media {
  public static void main(String[] args) {
    Collection collection = new ArrayList();
    DigitalVideoDisc dvd = new DigitalVideoDisc("BAKA", "Drama", 9.0f,
        1400, "Frank Darabont");
    collection.add(dvd);
    DigitalVideoDisc dvd2 = new DigitalVideoDisc("The Shawshank Redemption", "Drama", 9.0f,
        12, "Frank Darabont");
    collection.add(dvd2);
    DigitalVideoDisc dvd3 = new DigitalVideoDisc("Redemption", "Drama", 9.0f,
        142, "Frank Darabont");
    collection.add(dvd3);
    DigitalVideoDisc dvd4 = new DigitalVideoDisc("Meow", "Drama", 9.0f,
        149, "Frank Darabont");
    collection.add(dvd4);

    Iterator iterator = collection.iterator();
    // before sort
    System.out.println("----------------------------------------");
    System.out.println("The DVD list: ");
    while (iterator.hasNext()) {
      System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
    }
    System.out.println("----------------------------------------");
    // sort the collection
    Collections.sort((List) collection);

    // after sort
    System.out.println("----------------------------------------");
    System.out.println("The DVD list after sort: ");
    iterator = collection.iterator();
    while (iterator.hasNext()) {
      System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
    }
    System.out.println("----------------------------------------");
  }
}
