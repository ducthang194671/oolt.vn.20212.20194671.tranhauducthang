package hust.soict.hedspi.aims.test;

import hust.soict.hedspi.aims.media.Book;

import java.util.Collections;

public class BookTest {
    public static void main(String[] args) {
        Book book = new Book("The Lord of the Rings", "Fantasy", 9.0f, Collections.singletonList("J.R.R. Tolkien"), "The Lord of the Rings the Fellowship of the Ring");
        System.out.println(book.toString());
    }
}
