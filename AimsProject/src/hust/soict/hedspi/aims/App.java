package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.factory.BookCreation;
import hust.soict.hedspi.aims.media.factory.CDCreation;
import hust.soict.hedspi.aims.media.factory.DVDCreation;
import hust.soict.hedspi.aims.media.factory.MediaCreation;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.Wait;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
	private static List<Media> store = new ArrayList<Media>();
	private static Order order = null;
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		int choose = -1;
        do{
            System.out.println("\t\tYou are : ");
            System.out.println("\t\t1. Admin ");
            System.out.println("\t\t2. User ");
            System.out.println("\t\t0. Exit");
            System.out.println("\t\t--------------------------------");
            System.out.println("\t\tPlease choose a number: 0-1-2");
            choose = sc.nextInt();
            switch (choose) {
            case 1:
                admin();
                break;
            case 2:
                user();
                break;
            case 0:
                System.out.println("\t\tGoodbye");
                return;
            }
        } while(choose != 0);
	}

	public static void admin() {
		Scanner sc = new Scanner(System.in);
		int choose = -1;
		while (choose != 0) {
			showAdminMenu();
			choose = sc.nextInt();
			switch (choose){
				case 0:
					break;
				case 1:
					showSubMenu();
					Wait.pressAnyKeyToContinue();
					break;
				case 2:
					System.out.println("Input item's id you want to delete : ");
					int id = sc.nextInt();
					sc.nextLine();
					if (store.size() > id && id >= 0) {
						store.remove(id-1);
						System.out.println("Delete item successfully !!!");
					} else {
						System.out.println("Item's id is not exist !!!");
					}
					Wait.pressAnyKeyToContinue();
					break;
				case 3:
					if (store.size() > 0) {
						System.out.println("List of media in store : ");
						for (int i = 0; i < store.size(); i++) {
							System.out.println("\t\t" + (i+1) + ".\t" + store.get(i).toString());
						}
					} else System.out.println("There is no media in store");

					Wait.pressAnyKeyToContinue();
					break;

				default:
					System.out.println("Please choose 0 --> 3 !!!");
					break;

			}
		}
	}

	public static void user() {
		Scanner sc = new Scanner(System.in);
		int choose = -1;
		while (choose != 0) {
			showUserMenu();
			choose = sc.nextInt();
			sc.nextLine();
			switch (choose) {
				case 0:
					break;
				case 1:
					order = Order.createOrder();
					Wait.pressAnyKeyToContinue();
					break;
				case 2:

					String title = sc.nextLine();
					// check if title is exist in store
					boolean isExist = false;
					for(int i=0; i<store.size(); i++) {
						Media media = store.get(i);
						if (media.getTitle().equals(title)) {
							System.out.println("Founded !!!");
							System.out.println("\tID: " + i +  media.toString());
							isExist = true;
							break;
						}
					}
					if (isExist == false) {
						System.out.println("\t\tTitle is not exist in store");
					}
					Wait.pressAnyKeyToContinue();
					break;
				case 3:
                    if (order == null) {
                        System.out.println("Please create a order first !!!");
                    }
					System.out.println("Input item's id you want to add : ");
					int id = sc.nextInt();
					if (store.size() > id && id >= 0) {
						order.addMedia(store.get(id));
					} else {
						System.out.println("Item's id is not exist !!!");
					}
					Wait.pressAnyKeyToContinue();
				case 4:
                    if (order == null) {
                        System.out.println("Please create a order first !!!");
                    }
					System.out.println("Input item's id you want to remove : ");
					int id1 = sc.nextInt();
					order.removeMedia(id1);
					Wait.pressAnyKeyToContinue();
				case 5:
                    if (order == null) {
                        System.out.println("Please create a order first !!!");
                    }
					List<Media> list = order.getItemsOrdered();
					if (list.size() > 0) {
						System.out.println("List of media in order : ");
						for (int i = 0; i < list.size(); i++) {
							System.out.println("\t\t" + (i+1) + "." + list.get(i).toString());
						}
					} else System.out.println("There is no media in order");
					Wait.pressAnyKeyToContinue();
					break;
			}
		}

	}
	public static void showUserMenu() {
		System.out.println("\t\tWelcome to AIMS Store: ");
		System.out.println("\t\t--------------------------------");
		System.out.println("\t\t1. Create new order");
		System.out.println("\t\t2. Search for an item from the list by title");
		System.out.println("\t\t3. Add item to order by id (id in the list of available items of the store");
		System.out.println("\t\t4. Remove item from order by id (id in the order)");
		System.out.println("\t\t5. Display the order information");
		System.out.println("\t\t0. Exit");
		System.out.println("\t\t--------------------------------");
		System.out.println("\t\tPlease choose a number: 0-1-2-3-4-5");
	}
	public static void showAdminMenu() {
		System.out.println("Product Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new item");
		System.out.println("2. Delete item by id");
		System.out.println("3. Display the items list");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3");
	}
	public static void showSubMenu() {
		int choose = 0;
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("Choose media type : ");
			System.out.println("1. DVD");
			System.out.println("2. CD");
			System.out.println("3. BOOK");
			System.out.println("0. Exit");
			choose = sc.nextInt();
		} while (choose < 0 || choose > 3);
		switch (choose) {
			case 1 -> store.add(createMedia(new DVDCreation()));
			case 2 -> store.add(createMedia(new CDCreation()));
			case 3 -> store.add(createMedia(new BookCreation()));
			case 0 -> {
				return;
			}
		}
	}

	public static Media createMedia(MediaCreation mc) {
		return mc.createMediaFromConsole();
	}

}
