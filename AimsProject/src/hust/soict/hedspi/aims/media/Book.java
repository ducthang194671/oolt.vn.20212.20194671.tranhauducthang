package hust.soict.hedspi.aims.media;

import java.util.*;

public class Book extends Media{
    private List<String> authors = new ArrayList<String>();

    private String content;
    private List<String> contentTokens = new ArrayList<String>();
    private Map<String, Integer> wordFrequency = new HashMap<String, Integer>();

//    public Book(String title, String category, float cost) {
//        super(title, category, cost);
//    }

    public Book(String title, String category, float cost, List<String> authors, String content) {
        super(title, category, cost);
        this.authors = authors;
        this.setContent(content);
    }

    public void addAuthor(String authorName) {
        if (!authors.contains(authorName)) {
            authors.add(authorName);
        } else System.out.println("Duplicate authors !!!");
    }

    public void addAuthors(List<String> authors) {
        for (String author :
                authors) {
            if (!this.authors.contains(author)) this.authors.add(author);
            else {
                System.out.println(author + " is already added !!!");
            }
        }
    }

    public String toString() {
        return "\tBook" + super.toString() + "\t Authors: " + authors.toString() + "\tContent length:  " + contentTokens.size() + "\tWord frequency: " + wordFrequency.toString();
    }

    @Override
    public int compareTo(Media o) {
        return this.getTitle().compareTo(((Book) o).getTitle());
    }

    public void setContent(String content) {
        this.content = content;
        processContent();
    }

    private void processContent() {
        contentTokens.clear();
        wordFrequency.clear();
        String[] tokens = content.split(" |\\\\.");
        for (String token : tokens) {
            System.out.println(token);
            if (token.length() > 0) {
                contentTokens.add(token);
                if (wordFrequency.containsKey(token)) {
                    wordFrequency.put(token, wordFrequency.get(token) + 1);
                } else {
                    wordFrequency.put(token, 1);
                }
            }
        }
    }
}
