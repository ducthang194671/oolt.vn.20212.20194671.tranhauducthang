package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public abstract class Media implements Comparable<Media> {
    private int id;
    private String title;
    private String category;
    private float cost;

    public Media() {
        super();
    }

    public Media(String title) {
        this.title = title;
    }

    public Media(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
    }

    public String toString() {
        return  "\tTitle: " + title + "\tCategory: " + category + "\tPrice: " + cost;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        return this.id == ((Media) obj).getId();
    }

    @Override
    public int compareTo(Media o) {
        return this.getTitle().compareTo(((Media) o).getTitle());
    }
}
