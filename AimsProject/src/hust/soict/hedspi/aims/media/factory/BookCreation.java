package hust.soict.hedspi.aims.media.factory;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.Media;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BookCreation implements MediaCreation{
    @Override
    public Media createMediaFromConsole() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Input title : ");
        String title = sc.nextLine();
        System.out.println("Input category : ");
        String category = sc.nextLine();
        System.out.println("Input cost : ");
        float cost = sc.nextFloat();
        List<String> authors = new ArrayList<String>();
        int choose = -1;
        do {
            System.out.println("Do you want to add more author : ");
            System.out.println("1. Yes");
            System.out.println("0. No");
            choose = sc.nextInt();
            if (choose == 1) {
                System.out.println("Input author name : ");
                sc.nextLine();
                String name = sc.nextLine();
                if (!authors.contains(name)) authors.add(name);
                else System.out.println(name + " is already added !!!");
            }
        } while (choose != 0);
        sc.nextLine();
        System.out.println("Input content : ");
        String content = sc.nextLine();
        return new Book(title,category, cost, authors, content);
    }
}
