package hust.soict.hedspi.aims.media.factory;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.media.Media;

import java.util.Scanner;

public class CDCreation implements  MediaCreation{
    @Override
    public Media createMediaFromConsole() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input title : ");
        String title = sc.nextLine();
        System.out.println("Input category : ");
        String category = sc.nextLine();
        System.out.println("Input cost : ");
        float cost = sc.nextFloat();
        System.out.println("Input length : ");
        int length = sc.nextInt();
        sc.nextLine(); // skip \n
        System.out.println("Input directory : ");
        String directory = sc.nextLine();

        return new CompactDisc(title,category, cost, length, directory);
    }
}
