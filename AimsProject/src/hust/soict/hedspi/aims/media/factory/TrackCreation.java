package hust.soict.hedspi.aims.media.factory;

import hust.soict.hedspi.aims.disc.Track;

import java.util.Scanner;

public class TrackCreation {
    public static Track createTrackFromConsole(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Input title : ");
        String title = sc.nextLine();
        System.out.println("Input length : ");
        int length = sc.nextInt();
        sc.nextLine(); // skip \n

        return new Track(title, length);
    }
}
