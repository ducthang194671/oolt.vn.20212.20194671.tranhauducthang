package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Playable;
import hust.soict.hedspi.aims.media.factory.TrackCreation;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable{
    private String artist;
    private List<Track> tracks = new ArrayList<Track>();
    public CompactDisc(String title, String category, float cost, int length, String director) {
        super(title, category, cost, length, director);
    }

    public CompactDisc(String title, String category, float cost, int length, String director, String artist, List<Track> tracks) {
        super(title, category, cost, length, director);
        this.artist = artist;
        this.tracks = tracks;
    }

    public void addTrack() {
        Track trackFromConsole = TrackCreation.createTrackFromConsole();
        for(Track track: tracks) {
            if(track.getTitle().equals(trackFromConsole.getTitle())) {
                System.out.println("Track already exists");
                return;
            }
        }
        tracks.add(trackFromConsole);
    }

    public void removeTrack() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input track title: ");
        String title = sc.nextLine();
        for(Track track: tracks) {
            if(track.getTitle().equals(title)) {
                tracks.remove(track);
                return;
            }
        }
        System.out.println("Track not found");
    }

    @Override
    public void play() {
        for(Track track: tracks) {
            track.play();
        }
    }
    @Override
    public int getLength() {
        int totalLength = 0;
        for(Track track: tracks) {
            totalLength += track.getLength();
        }
        return totalLength;
    }

    public String toString() {
        return "\tCD"+super.toString();
    }

    public String getArtist() {
        return artist;
    }

    @Override
    public int compareTo(Media o) {
        return ((Integer)this.getLength()).compareTo(((Integer)((CompactDisc)o).getLength()));
    }
}
