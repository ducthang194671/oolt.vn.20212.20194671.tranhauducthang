package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Media;

import java.util.Scanner;

public abstract class Disc extends Media {
    private int length;
    private String director;

    public Disc(String title, String category, float cost, int length, String director) {
        super(title, category, cost);
        this.length = length;
        this.director = director;
    }

    @Override
    public String toString() {
        return super.toString() + "\tLength: " + length +"\tDirector: " + director;
    }

    public int getLength() {
        return length;
    }

    public String getDirector() {
        return director;
    }
}
