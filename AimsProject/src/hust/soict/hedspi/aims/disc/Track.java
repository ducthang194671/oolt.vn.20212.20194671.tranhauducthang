package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Playable;

public class Track implements Playable, Comparable<Track> {
    private String title;
    private int length;

    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }

    public String getTitle() {
        return title;
    }

    public int getLength() {
        return length;
    }

    @Override
    public void play() {
        System.out.println("Playing track: " + title);
        System.out.println("Track length: " + length);
    }

    @Override
    public boolean equals(Object obj) {
        return (this.title == ((Track) obj).getTitle()) && (this.length == ((Track) obj).getLength());
    }

    @Override
    public int compareTo(Track t) {
        return this.getTitle().compareTo(t.getTitle());
    }
}
