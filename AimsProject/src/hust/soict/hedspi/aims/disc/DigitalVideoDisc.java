package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.disc.Disc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Playable;

public class DigitalVideoDisc extends Disc implements Playable {

    public DigitalVideoDisc(String title, String category, float cost, int length, String director) {
        super(title, category, cost, length, director);
    }

    public String toString() {
        return "\tDVD" + super.toString();
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    @Override
    public int compareTo(Media o) {
        return ((Integer)this.getLength()).compareTo(((Integer)((DigitalVideoDisc)o).getLength()));
    }
}
